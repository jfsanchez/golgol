#!/usr/bin/env python

import os
import bottle
from bottle import run, debug
import logging

import controllers


def config_env():
    bottle.TEMPLATE_PATH = ['templates']
    DEBUG = True if os.getenv('GOLGOL_DEBUG', 'FALSE').upper() == 'TRUE' else False
    if DEBUG:
        logging.getLogger().setLevel(logging.INFO)
    debug(DEBUG)
    print('[DEBUG] =', DEBUG)


def main():
    config_env()
    run(host='localhost', port=8080)


if __name__ == '__main__':
    main()