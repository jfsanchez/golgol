import re
import requests
from queue import Queue
from urllib import parse
import logging

from models import Site, Word

RE_HTML_TAGS = re.compile(r'<.*?>')
RE_HTML_LINKS = re.compile(r'href="?\'?([^"\'>]*)')
RE_HTML_TITLE = re.compile(r'<title[^>]*>([^<]+)</title>')
RE_HTML_SCRIPT = re.compile(r'<script.*</script>')
RE_HTML_STYLE = re.compile(r'<style.*</style>')
RE_HTML_LINK = re.compile(r'<link.*>')
RE_HTML_SPECIALS = re.compile(r'&\w+;')
RE_SPECIALS = re.compile(r'(\n|\W+)')



def sanitize(html):
    html = RE_HTML_SCRIPT.sub(' ', html)
    html = RE_HTML_STYLE.sub(' ', html)
    html = RE_HTML_SPECIALS.sub(' ', html)
    html = RE_HTML_LINK.sub(' ', html)
    return html


def get_words(html):
    tmp = RE_HTML_TAGS.sub(' ', html.lower())
    tmp = RE_SPECIALS.sub(' ', tmp).rsplit()
    return tmp


def get_valid_link(link, base_url):
    p = parse.urlparse(link)
    if not p.netloc:
        path = p.geturl()
        link = parse.urljoin(base_url, path)
    return link


def get_links(html, base_url):
    tmp = RE_HTML_LINKS.findall(html)
    links = set([])
    for link in tmp:
        link = get_valid_link(link, base_url)
        links.add(link)
    return links


def get_title(html):
    title =  RE_HTML_TITLE.search(html).group(1)
    return title


def get_dict_words(words):
    d = {}
    for word in words:
        try:
            d[word] += 1
        except KeyError:
            d[word] = 1
    return d


def _clean_url(url):
    url[:url.index('#')] if '#' in url else url
    if not url.endswith('/'):
        url += '/'
    return url


def scrap(url, max_deep=3):
    q = Queue()
    q.put(url)
    q.put(0)
    tracked = set([])
    deep = 0
    sites = []

    while not q.empty() and deep < max_deep:
        url = q.get()
        deep = q.get()
        url = _clean_url(url)
        if url in tracked:
            continue
        tracked.add(url)
        logging.info('Processing {} [deep={}]'.format(url, deep))

        try:
            html = requests.get(url).text
            if not html:
                continue
            html = sanitize(html)
            title = get_title(html)
            links = get_links(html, url)
        except Exception as ex:
            logging.error('"{}" ({})'.format(url, str(ex)))
            continue
        dict_words = get_dict_words(get_words(html))
        words = [Word(name=word, count=dict_words[word]) for word in dict_words.keys()]
        site = Site(
            title=title,
            url=url,
            words=words,
        )
        sites.append(site)

        for link in links.difference(tracked):
            q.put(link)
            q.put(deep + 1)
    
    return sites