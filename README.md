How to start (Ubuntu/Linux):  
- Run locally a MongoDB instance on default port (27017).  
- Install virtualenv:  
$ sudo apt install virtualenv
- Create a new virtual environment:  
$ virtualenv -p python 3 golgol
- Create a src folder inside golgol directory:  
$ mkdir golgol/src/
- Move into src, clone this git project and then move into the project folder:  
$ cd golgol/src && git clone git@gitlab.com:jfsanchez/golgol.git && cd golgol  
- Install all necessary project dependences in the virtual environment:  
$ pip install -r requirements.txt
- Run the project:  
$ python3 main.py
- And now you can open your web browser at http://localhost:8080/  
