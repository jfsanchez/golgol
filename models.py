from mongoengine import Document, EmbeddedDocument, connect
from mongoengine import StringField, URLField, EmbeddedDocumentListField, ListField, IntField

DB_NAME = 'golgol_database'


class Word(EmbeddedDocument):
    name = StringField(required=True)
    count = IntField(min_value=0)

    def __str__(self):
        return '[{}]{}'.format(self.count, self.name)

    meta = {
        'ordering': 'count'
    }


class Site(Document):
    title = StringField(required=True)
    url = URLField(required=True, primary_key=True)
    words = EmbeddedDocumentListField(Word, ordering="count", reverse=True)

    def __str__(self):
        return '[{}] {}'.format(len(self.words), self.url)

    meta = {
        'ordering': ['-words__count']
    }

connect(DB_NAME)
