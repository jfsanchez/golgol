from functools import partial
import bottle
from bottle import route, get, post, request, redirect, url
from bottle import jinja2_template as template, jinja2_view as template_view
from urllib import parse
import scrapper

from models import Site, Word
from controllers.utils import message


@get('/index', name='index')
@template_view('index.html')
def index():
    return {}


@post('/do_index', name='do_index')
def do_index():
    q = request.POST.get('q', None)
    new_words = 0
    new_sites = 0
    if q:
        p = parse.urlparse(q)
        if not all([p.scheme, p.netloc]):
            raise Exception('Error: wrong URL "{}".'.format(q))
        del p
        sites = scrapper.scrap(q)
        for s in sites:
            try:
                site = Site.objects.get(url=s.url)
                old_len = len(site.words)
                site.update(set__words=s.words)
                site.reload()
                new_len = len(s.words)
                new_words += abs(new_len - old_len)
            except Site.DoesNotExist:
                s.save()
                new_sites += 1
                new_words += len(s.words)

        msg = 'Indexed {} new pages and {} words.'.format(
            new_sites,
            new_words,
        )
        message(request, msg)
    return redirect(url('index'))


@get('/')
def root():
    return redirect(url('index'))


@get('/clear', name='clear')
def clear():
    Site.objects.delete()
    msg = 'Index system is clean now.'
    message(request, msg)
    return redirect(url('index'))


@get('/search', name='search')
def search():
    ctx = {'count': -1}
    q = request.GET.get('q', '').lower()
    if q:
        sites = Site.objects(words__name=q).fields(
            title=1,
            url=1,
            words={'$elemMatch': {'name': q}}
        )
        sites = sorted(sites, key=cmp, reverse=True)
        ctx['sites'] = sites
        ctx['count'] = len(sites)
    return template('search.html', ctx)


def cmp(site):
    return site.words[0].count