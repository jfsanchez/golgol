import json
from bottle import request, get, post, redirect, HTTPResponse, response
from bottle import BaseTemplate, url


def pop_cookie(name):
    cookie = request.get_cookie(name)
    if cookie:
        response.set_cookie(name, '')
    return cookie


BaseTemplate.defaults['url'] = url
BaseTemplate.defaults['autoscape'] = True
BaseTemplate.defaults['request'] = request
BaseTemplate.defaults['pop_cookie'] = pop_cookie


from controllers.statics_controller import *
from controllers.home_controller import *
