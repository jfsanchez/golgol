from bottle import response


def message(request, message):
    response.set_cookie('message', message)
    return response
