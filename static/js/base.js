$(document).ready(function(){
    function active_menu() {
        let path = location.pathname;
        let selector = `a[path="${path}"]`;
        let a = $(selector);
        a.addClass('active');
    }
    active_menu();
});